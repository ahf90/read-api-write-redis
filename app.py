import json
import os
from prometheus_client import start_http_server, Counter
import redis
import requests

REQUEST_METHOD = os.getenv('REQUEST_METHOD', 'GET')
API_URL = os.getenv('API_URL')
REDIS_SERVICE_HOST = os.getenv('REDIS_SERVICE_HOST')
REDIS_SERVICE_PORT = os.getenv('REDIS_SERVICE_PORT', 6379)
REDIS_DB_NUM = os.getenv('REDIS_DB_NUM', 0)
REDIS_KEY = os.getenv('REDIS_KEY', 'main')
SUCCESSFUL_API_REQUESTS = Counter('successful_api_requests', '# of successful API requests')
REDIS_INSERTS = Counter('games_upserted', '# inserts into Redis')


def check_env_vars():
    for global_var in [("REDIS_SERVICE_HOST", REDIS_SERVICE_HOST), ("API_URL", API_URL)]:
        var_name, value = global_var[0], global_var[1]
        if not value:
            raise KeyError(f'Missing environment variable: {var_name}')


def dict_from_env_vars(prefix):
    keys = dict(os.environ).keys()
    data = {}
    for key in keys:
        if key.startswith(prefix):
            data.update({key.split(f'{prefix}_')[1]: os.environ.get(key)})
    return data


def post_from_api(headers, post_data):
    return requests.post(API_URL, headers=headers, data=post_data).json()


def get_from_api(headers, params):
    res = requests.get(API_URL, headers=headers, params=params)
    if res.status_code == 200:
        SUCCESSFUL_API_REQUESTS.inc()
    return res.json()


def write_to_redis(data):
    r = redis.Redis(host=REDIS_SERVICE_HOST, port=REDIS_SERVICE_PORT, db=REDIS_DB_NUM)
    r.rpush(REDIS_KEY, json.dumps(data))
    REDIS_INSERTS.inc()


if __name__ == '__main__':
    start_http_server(8000)
    check_env_vars()

    HEADERS = dict_from_env_vars('HTTP_HEADER')
    if REQUEST_METHOD == 'POST':
        POST_DATA = dict_from_env_vars('POST_DATUM')
        API_DATA = post_from_api(HEADERS, POST_DATA)
    else:
        PARAMS = dict_from_env_vars('GET_PARAM')
        API_DATA = get_from_api(HEADERS, PARAMS)
    write_to_redis(API_DATA)
